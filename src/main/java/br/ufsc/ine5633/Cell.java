package br.ufsc.ine5633;

public class Cell {
    private Seed content;
    private Integer coordX;
    private Integer coordY;

    public Cell(int x, int y) {
        this.coordX = x;
        this.coordY = y;
        this.content = Seed.EMPTY;
    }

    public Seed getContent() {
        return content;
    }

    public Cell setContent(Seed content) {
        this.content = content;
        return this;
    }

    public Integer getCoordX() {
        return coordX;
    }

    public Cell setCoordX(Integer coordX) {
        this.coordX = coordX;
        return this;
    }

    public Integer getCoordY() {
        return coordY;
    }

    public Cell setCoordY(Integer coordY) {
        this.coordY = coordY;
        return this;
    }
}
