package br.ufsc.ine5633;

/**
 * Created by NildoJunior on 18/05/2016.
 */
public class GameMain {

    public static final int ROWS = 6;
    public static final int COLS = 7;
    public static final String MSG_CHOOSE_COLUMN = "Escolha o número da coluna para inserir uma peça: ";
    public static final String MSG_INVALID = "Inválido!";
}
