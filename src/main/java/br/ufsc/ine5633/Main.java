package br.ufsc.ine5633;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

import static br.ufsc.ine5633.GameMain.COLS;
import static br.ufsc.ine5633.GameMain.ROWS;
import static java.lang.String.format;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Board board = new Board(COLS, ROWS);
        Boolean hasWinner = false;

        while (!hasWinner) {
            printBoard(board);
            Integer chosenColumn = readInput(scan);
            board.addInColumn(chosenColumn, Seed.PLAYER_1); //TODO tratar cada simbolo
        }
        scan.close();
    }

    private static Integer readInput(Scanner scan) {
        String input = null;
        boolean valid = false;

        while (input == null || !valid) {
            System.out.print(GameMain.MSG_CHOOSE_COLUMN);
            input = scan.next();

            valid = input.matches("[1-7]");
            if (!valid) {
                System.out.println(GameMain.MSG_INVALID);
            }
        }

        return Integer.valueOf(input);
    }

    private static void printBoard(Board board) {
        String[] linesStr = new String[ROWS];
        printNumbers();
        for (int x = 0; x < board.getCells().length; x++) {
            Cell[] column = board.getCells()[x];
            for (int y = 0; y < column.length; y++) {
                String end = x == (board.getCells().length - 1) ? "|" : "";
                String line = linesStr[y];
                line = line != null ? line : "";
                linesStr[y] = format("%s|%s%s", line, column[y].getContent().getValue(), end);
            }
        }

        Arrays.stream(linesStr).forEach(System.out::println);
        printNumbers();
    }

    private static void printNumbers() {
        IntStream.rangeClosed(1, COLS)
                .forEach(value -> System.out.print(format(" %d", value)));
        System.out.println();
    }

}
