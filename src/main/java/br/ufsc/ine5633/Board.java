package br.ufsc.ine5633;

/**
 * Created by NildoJunior on 18/05/2016.
 */
public class Board {

    private Cell[][] cells;

    public Board(int nColumns, int nLines) {
        cells = new Cell[nColumns][nLines];
        for (int x = 0; x < cells.length; x++) {
            Cell[] column = cells[x];
            for (int y = 0; y < column.length; y++) {
                cells[x][y] = new Cell(x, y);
            }
        }
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Board setCells(Cell[][] cells) {
        this.cells = cells;
        return this;
    }


    public void addInColumn(Integer chosenColumn, Seed value) {
        Integer columnIndex = chosenColumn - 1;
        Cell[] column = this.cells[columnIndex];
        for (int rowIndex = column.length - 1; rowIndex >= 0; rowIndex--) {
            if (column[rowIndex].getContent().getValue().equals(Seed.EMPTY.getValue())) {
                this.cells[columnIndex][rowIndex].setContent(value);
                break;
            }
        }
    }
}
